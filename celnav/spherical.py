from .angle import Angle
import math

def suffix_to_sign(angle_str: str) -> str:
    angle_str = angle_str.strip()
    if angle_str[-1] in ('N', 'E'):
        return angle_str[:-1]
    elif angle_str[-1] in ('S', 'W'):
        if angle_str[0] == '-':
            return angle_str[1:-1]
        else:
            return '-' + angle_str[:-1]
    else:
        return angle_str

AnglePair = tuple[Angle, Angle]

class Pos(AnglePair):
    def lat(self):
        return self[0]

    def lon(self):
        return self[1]

    def __format__(self, format_spec):
        return format(abs(self.lat()), format_spec) + (' N, ' if self.lat() >= 0 else ' S, ') \
               + format(abs(self.lon()), format_spec) + (' E' if self.lon() >= 0 else ' W')

    def __str__(self):
        return format(self)

    def __new__(self, pair_or_x, y=None):
        if y == None:
            if (type(pair_or_x) == str):
                if (sep := pair_or_x.find(',')) != -1:
                    lat = suffix_to_sign(pair_or_x[:sep])
                    lon = suffix_to_sign(pair_or_x[sep+1:])
                elif (sep := pair_or_x.find('S')) != -1:
                    lat = suffix_to_sign(pair_or_x[:sep+1])
                    lon = suffix_to_sign(pair_or_x[sep+1:])
                elif (sep := pair_or_x.find('N')) != -1:
                    lat = suffix_to_sign(pair_or_x[:sep+1])
                    lon = suffix_to_sign(pair_or_x[sep+1:])
                else:
                    raise ValueError("cannot parse spherical coordinates from string: " + pair_or_x)
                return tuple.__new__(self, (Angle(lat), Angle(lon)))
            else:
                return tuple.__new__(self, pair_or_x)
        else:
            return tuple.__new__(self, (pair_or_x, y))

    def __add__(self, pos):
        return Pos(self.lat() + pos.lat(), self.lon() + pos.lon())

class HorizontalPos(AnglePair):
    def alt(self):
        return self[0]

    def az(self):
        return self[1]

    def __format__(self, format_spec):
        fmt_alt, sep, fmt_az = format_spec.partition(',')
        if sep == '':
            fmt_alt = fmt_az = format_spec
        return format(abs(self.alt()), fmt_alt) + ', ' + format(abs(self.az()), fmt_az)

    def __str__(self):
        return format(self)

    def __new__(self, pair_or_x, y=None):
        if y == None:
            if (type(pair_or_x) == str):
                lat, sep, lon = pair_or_x.partition(',')
                if sep != ',':
                    raise ValueError("cannot parse spherical coordinates from string: " + pair_or_x)
                return tuple.__new__(self, (Angle(lat), Angle(lon)))
            else:
                return tuple.__new__(self, pair_or_x)
        else:
            return tuple.__new__(self, (pair_or_x, y))

def alt_az(observer: Pos, body:Pos) -> HorizontalPos:
    """Return the horizontal coordinates of body as seen from observer"""
    lha = (observer.lon() - body.lon()).normalised()
    sin_alt = math.sin(observer.lat()) * math.sin(body.lat()) \
              + math.cos(observer.lat()) * math.cos(body.lat()) * math.cos(lha)
    alt = Angle.asin(sin_alt)
    azimuth = Angle.acos((math.sin(body.lat()) - sin_alt * math.sin(observer.lat()))
                         / (math.cos(alt) * math.cos(observer.lat())))
    if lha < Angle.d180():
        azimuth = Angle.d360() - azimuth
    return HorizontalPos(alt, azimuth)

class Circle:
    def __init__(self, centre: Pos, radius: Angle):
        # Note: For circles resulting from observations, radius is equal to the zenith distance
        self.centre = centre
        self.radius = radius
