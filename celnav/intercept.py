from . import sight
from . import spherical
from . import lop
from .angle import Angle
from .base import *
from .gp import *

import datetime
import sys

CRED = '\033[91m'
CGREEN = '\033[92m'
CEND = '\033[0m'

def print_error(string):
    print(CRED + string + CEND, file=sys.stderr)

class StHilaireSight:
    """Helper class to keep track of variables used in the St. Hilaire intercept method."""
    def __init__(self, body_name: str, env:sight.Environment = sight.Environment()):
        self.body = Body.from_name(body_name)
        self.sight = sight.Sight(self.body, env)
        self.time = None
        self.gp: None|spherical.Pos = None
        self.default_date = ts.now().utc_datetime().date()

        self.observed_alt: None|Angle = None
        self.circle_of_position: None|lop.CircleOfPosition = None

        self.ap: None|spherical.Pos = None
        self.computed_alt_az: None|spherical.HorizontalPos = None

    def interactive_shell(self):
        def get_new_command():
            try:
                while not (x := input(CGREEN + f"[{self.body.name}] " + CEND)): pass
                return x
            except EOFError:
                print('')
                return ''
        while command := get_new_command().strip():
            tokens = command.split()
            assert len(tokens) > 0
            if command[0] == 'h':
                print("Commands:")
                print(f" Ctrl+D: Finish editing {self.body.name} sight for now")
                print(" h[help]: Show this help message")
                print(" p[rint]: Print information about this sight/LOP")
                print(" s[et] e[nv]: Interactively set environment")
                print(" s[et] i[ndex_error]|h[oe]|t[emp]|p[res] value: Set single value directly")
                print(" s[et] a[ssumed_position] [value]: Set assumed position")
                print(" l[ist]: List altitude measurements")
                print(" a[dd]: Add altitude measurement")
                print(" r[emove] i: Remove altitude measurement number i")
                print(" c[omment] i comment: Set comment for altitude measurement number i")
            elif command[0] == "p":
                self.update_and_print()
            elif command[0] == "s":
                if len(tokens) < 2:
                    print_error("Error: Please specify which variable to set")
                    continue
                var = tokens[1][0]
                if var not in ['e', 'c', 'a']:
                    try:
                        val = float(tokens[2])
                    except IndexError:
                        print_error("Error: Please supply value")
                        continue
                    except ValueError:
                        print_error("Error: Invalid value: " + tokens[2])
                        continue
                if var == 'e':
                    self.interactive_set_environment()
                elif var == 'a':
                    if len(tokens) == 2:
                        lat = Angle(spherical.suffix_to_sign(input(
                            "AP Latitude [deg.deg°, deg°min.min' or deg°min'sec\" N|S]: ")))
                        lon = Angle(spherical.suffix_to_sign(input(
                            "AP Longitude [deg.deg°, deg°min.min' or deg°min'sec\" E|W]: ")))
                        self.ap = spherical.Pos(lat, lon)
                    else:
                        try:
                            self.ap = spherical.Pos(",".join(tokens[2:]))
                        except ValueError:
                            print_error("Error: Could not parse position: " + ",".join(tokens[2:]))
                elif var == 'i':
                    self.sight.env.index_error = Angle.from_minutes(val)
                elif var == 'h':
                    self.sight.env.height_of_eye = Metre(val)
                elif var == 't':
                    self.sight.env.temperature = sight.Environment.Temperature_K.from_celsius(val)
                elif var == 'p':
                    self.sight.env.pressure = sight.Environment.Pressure_hPa(val)
                else:
                    print_error("Error: Unrecognised variable: " + var)
            elif command[0] == "l":
                self.update_all()
                for i, altitude_measurement in enumerate(self.sight.sights):
                    print(f"{i:3d}:", altitude_measurement)
            elif command[0] == "a":
                self.interactive_add_sight()
            elif command[0] == "r":
                if len(tokens) == 1:
                    tokens.append(input("Remove sight number: "))
                try:
                    self.sight.sights.pop(int(tokens[1]))
                except (ValueError, IndexError):
                    print_error("Error: Invalid sight number: " + tokens[1])
            elif command[0] == "c":
                if len(tokens) == 1:
                    print_error("Error: Please supply altitude measurement number")
                    continue
                try:
                    self.sight.sights[int(tokens[1])].comment = " ".join(tokens[2:])
                except (ValueError, IndexError):
                    print_error("Error: Invalid sight number: " + tokens[1])
            else:
                print_error("Error: Unrecognised command: " + command)

    def interactive_set_environment(self):
        self.sight.env.index_error = Angle.from_minutes(float(input("Index error ['] = ")))
        self.sight.env.height_of_eye = Metre(float(input("Height of eye [m] = ")))
        self.sight.env.temperature = sight.Environment.Temperature_K.from_celsius(
                float(input("Air temperature [°C] = ")))
        self.sight.env.pressure = sight.Environment.Pressure_hPa(float(input("Air pressure [hPa] = ")))

    def interactive_add_sights(self):
        while (input("Add another altitude measurement [Y/n]? ") or 'Y').upper() == 'Y':
            self.interactive_add_sight()

    def interactive_add_sight(self):
        while True:
            try:
                time_string = input("Time [UTC]: ")
            except EOFError:
                print("")
                return
            if '.' not in time_string:
                time_string += '.0'
            try:
                time = datetime.datetime.strptime(time_string, "%H:%M:%S.%f").time()
                break
            except ValueError:
                print_error("Error: Time not in valid HH:MM:SS.S format")
                continue
        while True:
            try:
                date = datetime.datetime.strptime(
                        input(f"Date [YYYY-MM-DD, default {self.default_date}]: ") or f"{self.default_date}",
                        "%Y-%m-%d").date()
                break
            except ValueError:
                print_error("Error: Date not in valid YYYY-MM-DD format")
                continue
            except EOFError:
                print("")
                return
        if self.sight.body.body_type in [BodyType.SUN, BodyType.MOON, BodyType.PLANET]:
            while True:
                try:
                    limb = sight.Limb.from_char(input("Limb [l|C|u]: ") or 'c')
                    break
                except KeyError:
                    print_error("Error: Please specify l, c or u")
                    continue
                except EOFError:
                    print("")
                    return
        else:
            limb = sight.Limb.CENTRE
        while True:
            try:
                angle = Angle(input("Sextant altitude [deg°min'sec\" or deg°min.min']: "))
                break
            except (ValueError, IndexError):
                print_error("Error: Invalid angle")
                continue
            except EOFError:
                print("")
                return
        self.sight.sights.append(
            sight.AltitudeMeasurement(limb,
                                      angle,
                                      ts.from_datetime(
                                          datetime.datetime.combine(date, time, datetime.UTC)
                                      ),
                                      input("Comment (optional): ") or ""))
        
#    def interactive_set_almanac_data(self):
#        self.gp = spherical.Pos(Angle(spherical.suffix_to_sign(input("Dec [deg°min.min'N|S]: "))),
#                                Angle.d360() - Angle(input("GHA [deg°min.min']: ")))
#        if self.sight.body().body_type in (Body.MOON, Body.PLANET):
#            self.sight.set_hp(Angle.from_minutes(float(input("HP [']: "))))
#        if self.sight.body().body_type == Body.SUN:
#            self.sight.set_sd(Angle.from_minutes(float(input("SD [']: "))))

    def compute_gp(self):
        assert self.time != None
        body = self.body.at(self.time)
        self.gp = body.gp()

    def update_all(self):
        if len(self.sight.sights):
            self.compute_circle_of_position()
            if self.ap:
                self.compute_lop_from_ap()

    def update_and_print(self):
        self.update_all()
        self.print()

#    def add_almanac_data(self, gp: spherical.Pos, hp: None|Angle = None, sd: None|Angle = None):
#        self.gp = gp
#        if self.sight.body() in (Body.MOON, Body.PLANET):
#            assert(type(hp) == Angle)
#            self.sight.set_hp(hp)
#        if self.sight.body() == Body.SUN:
#            assert(type(sd) == Angle)
#            self.sight.set_sd(sd)

    def compute_circle_of_position(self):
        self.observed_alt, self.time = self.sight.average_corrected_sight()
        self.compute_gp()
        self.circle_of_position = lop.CircleOfPosition(
                spherical.Circle(self.gp, Angle.d90() - self.observed_alt),
                self.time)

    def compute_lop_from_ap(self):
        self.computed_alt_az = spherical.alt_az(self.ap, self.gp)

    def print(self):
        def maybe_format(x, fmt):
            if x != None:
                return format(x, fmt)
            else:
                return "None"
        self.sight.print()
        if self.circle_of_position == None:
            print("Average time: None")
        else:
            print("Average time: {} UTC".format(self.circle_of_position.time.utc_datetime()))
        print("Ho =", maybe_format(self.observed_alt, "ddm:.1f"))
        #print("Default date:", self.default_date)
        print("----------------------")
        print("Geographical position:", maybe_format(self.gp, "ddm:.2f"))
        if self.gp:
            if self.body.body_type == BodyType.STAR:
                print("(GHA_Aries = {:ddm:.1f}, SHA = {:ddm:.1f}, ".format(
                        Angle.from_degrees(15 * self.time.gast),
                        Angle.from_degrees(360 - 15 * self.body.at(self.time).ra.hours)),
                      end='')
            else:
                print("(", end='')
            print("GHA = {:ddm:.1f}, Dec = {:ddm:.1f})".format(Angle.d360() - self.gp.lon().normalised(), self.gp.lat()))
        #print("Circle of Position:", self.circle_of_position)
        print("----------------------")
        print("Assumed Position:", maybe_format(self.ap, "ddm:.2f"))
        print("Hc, Zn =", maybe_format(self.computed_alt_az, "ddm:.1f,dd:.1f"))
        print("ΔH =", maybe_format(self.delta_h(), "min:.2f"))

    def one_line_description(self):
        if len(self.sight.sights) == 0:
            sights_descr = "no sights"
        elif len(self.sight.sights) == 1:
            sights_descr = "single sight"
        else:
            sights_descr = f"average over {len(self.sight.sights)} sights"
        return f"{self.sight.body.name}, {sights_descr}"

    def delta_h(self):
        if self.observed_alt != None and self.computed_alt_az != None:
            return self.observed_alt - self.computed_alt_az.alt()
        else:
            return None

    def num_sights(self):
        return len(self.sight.sights)
