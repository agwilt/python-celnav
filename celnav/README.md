# Celestial Navigation Package

## Modules:

### angle
Just contains the `Angle` class. `Angle` is just a float representing an angle in radians format, with some
nice conversion and type-safe operator functions.

### base
Universal, basic definitions like `Body`

### intercept
Contains the `StHilaireSight` class, which just keeps track of relevant data for a sight reduction, as well
as providing interactive data manipulation methods (currently used by `interactive_celnav`).

### lop
`LineOfPosition` and `CircleOfPosition` classes, with functions to work with them

### sight
Classes `Environment`, `AltitudeMeasurement` and `Sight`.
 * `Environment` refers to the circumstances in which a sextant sight was performed, so it contains
   `index_error`, `height_of_eye`, `temperature` and `pressure`
 * `AltitudeMeasurement` is a single sextant sight of a celestial body, storing `limb`, `sextant_altitude` and `time`.
 * `Sight` stores a list of `AltitudeMeasurement`s that can be averaged/interpolated for better accuracy, together
   with data common to the whole, summarised sight.

### spherical
Module for spherical trigonometry stuff. Contains classes `Pos` (lat/lon), `HorizontalPos` (alt/az) and
`Circle` (defined by centre and radius).
