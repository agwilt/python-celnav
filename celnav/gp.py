from skyfield.named_stars import named_star_dict
import skyfield.api
from skyfield.data import hipparcos
from .base import *
from .angle import Angle
from .spherical import Pos

named_star_dict['Acamar'] = 13847
named_star_dict['Eltanin'] = 87833
named_star_dict['Menkar'] = 14135
named_star_dict['Zubenelgenubi'] = 72622

planet_names = {'Mercury':'mercury', 'Venus':'venus', 'Mars':'mars', 'Jupiter':'jupter barycenter', 'Saturn':'saturn barycenter'}

planets = skyfield.api.load('de421.bsp')
earth = planets['earth']

with skyfield.api.load.open(hipparcos.URL) as f:
    hip_dataframe = hipparcos.load_dataframe(f)

body_names = list(named_star_dict.keys()) + list(planet_names.keys()) + ['Sun', 'Moon']

class Body:
    def __init__(self, body_type: BodyType, body_name = None):
        self.body_type = body_type
        if body_type == BodyType.SUN:
            assert body_name == None
            self._body = planets['Sun']
            self.name = 'Sun'
        elif body_type == BodyType.MOON:
            assert body_name == None
            self._body = planets['Moon']
            self.name = 'Moon'
        elif body_type == BodyType.STAR:
            try:
                self._body = skyfield.api.Star.from_dataframe(hip_dataframe.loc[named_star_dict[body_name]])
            except KeyError:
                raise ValueError("Unknown star: " + body_name)
            self.name = body_name
        elif body_type == BodyType.PLANET:
            assert type(body_name) == str
            try:
                self._body = planets[body_name]
            except KeyError:
                self._body = planets[body_name.upper() + " BARYCENTER"]
            self.name = body_name
        else:
            assert False

    @staticmethod
    def from_name(name: str):
        name = name.capitalize()
        if name == 'Sun':
            return Body(BodyType.SUN)
        elif name == 'Moon':
            return Body(BodyType.MOON)
        elif name in planet_names:
            return Body(BodyType.PLANET, name)
        else:
            # must be a star
            return Body(BodyType.STAR, name)

    def mean_radius_km(self):
        if self.body_type == BodyType.SUN:
            return 695700.0
        elif self.body_type == BodyType.MOON:
            return 1737.4
        elif self.body_type == BodyType.STAR:
            return 0.0
        elif self.body_type == BodyType.PLANET:
            if self.name == "Mercury":
                return 2439.7
            elif self.name == "Venus":
                return 6051.8
            elif self.name == "Mars":
                return 3389.5
            elif self.name == "Jupiter":
                return 69911
            elif self.name == "Saturn":
                return 58232
            else:
                raise ValueError("Radius of planet " + self.name + " unknown")
        else:
            assert False

    def at(self, time):
        return BodyAtTime(self, time)

class BodyAtTime:
    def __init__(self, body:Body, time):
        self.body = body
        self.time = time
        self.apparent_pos = earth.at(time).observe(body._body).apparent()
        self.ra, self.dec, self.dist = self.apparent_pos.radec('date')

    def gp(self) -> Pos:
        lon = Angle.from_degrees(15 * (self.ra.hours - self.time.gast))
        while lon > Angle.d180():
            lon -= Angle.d360()
        while lon <= -Angle.d180():
            lon += Angle.d360()
        return Pos(Angle(self.dec.radians), lon)

    def sd(self) -> Angle:
        if self.body.body_type == BodyType.STAR:
            return Angle(0)
        else:
            return Angle.atan(self.body.mean_radius_km() / self.dist.km)

    def hp(self) -> Angle:
        if self.body.body_type == BodyType.STAR:
            return Angle(0)
        else:
            return Angle.atan(6371.0 / self.dist.km)
