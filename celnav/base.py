import enum
import typing
import skyfield.api

BodyType = enum.Enum('BodyType', ['SUN', 'STAR', 'PLANET', 'MOON'])

Metre = typing.NewType('Metre', float)

ts = skyfield.api.load.timescale()
