from .base import *
from . import spherical
from .angle import Angle
from .base import *
from .gp import Body, BodyAtTime
from . import sight
from skyfield.timelib import Time

import datetime
import sys
import math
import statistics
import numpy as np

ld_limb_names = {'n':"near", 'f':"far"}

class DistanceMeasurement:
    def __init__(self, angle: Angle, time: Time, limb: str):
        self.angle = angle
        self.time = time
        self.limb = limb # "n" for near, "f" for far
    def __str__(self):
        return "{} limb, {:dms:02.0f} at {} UTC".format(
                ld_limb_names[self.limb], self.angle, self.time.utc_datetime()
            )
    def angle_corrected_for_sd(self, sd: Angle):
         return self.angle + {'n':sd, 'f':-sd}[self.limb]

class AltitudeMeasurement:
    def __init__(self, body: Body):
        self.body = body
        self.measured: None|sight.AltitudeMeasurement = None
        self.alt: None|sight.ObservedAltitude = None
    def __str__(self):
        return self.alt.__str__()
    def correct(self, env: sight.Environment):
        if self.measured != None:
            assert type(self.measured) == sight.AltitudeMeasurement
            self.alt = sight.ObservedAltitude(self.measured, env, self.body)
    def interactive_set(self, default_date):
        while True:
            try:
                time_string = input("Time [UTC]: ")
            except EOFError:
                print("")
                return
            if '.' not in time_string:
                time_string += '.0'
            try:
                time = datetime.datetime.strptime(time_string, "%H:%M:%S.%f").time()
                break
            except ValueError:
                print_error("Error: Time not in valid HH:MM:SS.S format")
                continue
        while True:
            try:
                date = datetime.datetime.strptime(
                        input(f"Date [YYYY-MM-DD, default {default_date}]: ") or f"{default_date}",
                        "%Y-%m-%d").date()
                break
            except ValueError:
                print_error("Error: Date not in valid YYYY-MM-DD format")
                continue
            except EOFError:
                print("")
                return
        if self.body.body_type in [BodyType.SUN, BodyType.MOON, BodyType.PLANET]:
            while True:
                try:
                    limb = sight.Limb.from_char(input("Limb [l|C|u]: ") or 'c')
                    break
                except KeyError:
                    print_error("Error: Please specify l, c or u")
                    continue
                except EOFError:
                    print("")
                    return
        else:
            limb = sight.Limb.CENTRE
        while True:
            try:
                angle = Angle(input("Sextant altitude [deg°min'sec\" or deg°min.min']: "))
                break
            except (ValueError, IndexError):
                print_error("Error: Invalid angle")
                continue
            except EOFError:
                print("")
                return
        self.measured = sight.AltitudeMeasurement(limb,
                                                  angle,
                                                  ts.from_datetime(
                                                      datetime.datetime.combine(date, time, datetime.UTC)),
                                                  input("Comment (optional): ") or "")


CRED = '\033[91m'
CGREEN = '\033[92m'
CEND = '\033[0m'

def print_error(string):
    print(CRED + string + CEND, file=sys.stderr)

class LunarDistance:
    def __init__(self, body_name):
        self.body = Body.from_name(body_name)
        self.moon = Body.from_name("Moon")
        self.env = sight.Environment()
        self.default_date = ts.now().utc_datetime().date()

        self.lunar_alt = AltitudeMeasurement(self.moon)
        self.body_alt = AltitudeMeasurement(self.body)
        self.distance_measurements = []

        self.chronometer_time = None
        self.uncleared_avg_ld = None
        self.cleared_avg_ld = None

        self.gmt = None # time computed from ld

    def interactive_shell(self):
        def get_new_command():
            try:
                while not (x := input(CGREEN + f"[Lunar Distance] " + CEND)): pass
                return x
            except EOFError:
                print('')
                return ''
        while command := get_new_command().strip():
            tokens = command.split()
            assert len(tokens) > 0
            if command[0] == 'h':
                print("Commands:")
                print(f" Ctrl+D: Finish editing {self.body.name} sight for now")
                print(" h[help]: Show this help message")
                print(" p[rint]: Print information about this lunar")
                print(" s[et] e[nv]: Interactively set environment")
                print(" s[et] i[ndex_error]|h[oe]|t[emp]|p[res] value: Set single value directly")
                print(" l[ist]: List measurements")
                print(f" a[dd] Moon|{self.body.name}: Set measured altitude")
                print(" a[dd] LD: Add measurement")
                print(" r[emove] i: Remove measurement number i")
            elif command[0] == "p":
                self.update_and_print()
            elif command[0] == "s":
                if len(tokens) < 2:
                    print_error("Error: Please specify which variable to set")
                    continue
                var = tokens[1][0]
                if var not in ['e', 'c', 'a']:
                    try:
                        val = float(tokens[2])
                    except IndexError:
                        print_error("Error: Please supply value")
                        continue
                    except ValueError:
                        print_error("Error: Invalid value: " + tokens[2])
                        continue
                if var == 'e':
                    self.interactive_set_environment()
                elif var == 'i':
                    self.env.index_error = Angle.from_minutes(val)
                elif var == 'h':
                    self.env.height_of_eye = Metre(val)
                elif var == 't':
                    self.env.temperature = sight.Environment.Temperature_K.from_celsius(val)
                elif var == 'p':
                    self.env.pressure = sight.Environment.Pressure_hPa(val)
                else:
                    print_error("Error: Unrecognised variable: " + var)
            elif command[0] == "l":
                self.update_all()
                print("Moon:", self.lunar_alt)
                print(f"{self.body.name}:", self.body_alt)
                print("LD:")
                for ld in self.distance_measurements:
                    print(f" {index:3d}:", ld)
                    index += 1
            elif command[0] == "a":
                if len(tokens) == 1:
                    tokens.append(input(f"Add measurement type: [Moon|{self.body.name}|LD] "))
                if tokens[1] == "Moon":
                    self.lunar_alt.interactive_set(self.default_date)
                elif tokens[1] == self.body.name:
                    self.body_alt.interactive_set(self.default_date)
                elif tokens[1] == "LD":
                    self.interactive_add_ld_measurement()
                else:
                    print_error(f"Error: Measurement type must be Moon, {self.body.name} or LD")
                    continue
            elif command[0] == "r":
                if len(tokens) == 1:
                    tokens.append(input("Remove measurement number: "))
                try:
                    self.distance_measurements.pop(int(tokens[1]))
                except (ValueError, IndexError):
                    print_error("Error: Invalid measurement number: " + tokens[1])
            else:
                print_error("Error: Unrecognised command: " + command)

    def interactive_set_environment(self):
        self.env.index_error = Angle.from_minutes(float(input("Index error ['] = ")))
        self.env.height_of_eye = Metre(float(input("Height of eye [m] = ")))
        self.env.temperature = sight.Environment.Temperature_K.from_celsius(
                float(input("Air temperature [°C] = ")))
        self.env.pressure = sight.Environment.Pressure_hPa(float(input("Air pressure [hPa] = ")))

    def interactive_add_ld_measurement(self):
        while True:
            try:
                time_string = input("Time [UTC]: ")
            except EOFError:
                print("")
                return
            if '.' not in time_string:
                time_string += '.0'
            try:
                time = datetime.datetime.strptime(time_string, "%H:%M:%S.%f").time()
                break
            except ValueError:
                print_error("Error: Time not in valid HH:MM:SS.S format")
                continue
        while True:
            try:
                date = datetime.datetime.strptime(
                        input(f"Date [YYYY-MM-DD, default {self.default_date}]: ") or f"{self.default_date}",
                        "%Y-%m-%d").date()
                break
            except ValueError:
                print_error("Error: Date not in valid YYYY-MM-DD format")
                continue
            except EOFError:
                print("")
                return
        while True:
            try:
                limb = input("Limb (near or far) [N|f]: ").lower() or 'n'
                break
            except KeyError:
                print_error("Error: Please specify n or f")
                continue
            except EOFError:
                print("")
                return
        while True:
            try:
                angle = Angle(input("Measured lunar distance [deg°min'sec\" or deg°min.min']: "))
                break
            except (ValueError, IndexError):
                print_error("Error: Invalid angle")
                continue
            except EOFError:
                print("")
                return
        self.distance_measurements.append(
            DistanceMeasurement(angle,
                                ts.from_datetime(datetime.datetime.combine(date, time, datetime.UTC)),
                                limb))

    def compute_avg_ld_measurement(self):
        semidiameters = self.lunar_alt.alt.sd_topo() + self.body_alt.alt.sd_topo()
        assert len(self.distance_measurements)
        if len(self.distance_measurements) == 1:
            return self.distance_measurements[0].angle_corrected_for_sd(semidiameters), self.distance_measurements[0].time
        else:
            times = [ld.time.tt for ld in self.distance_measurements]
            angles = [ld.angle_corrected_for_sd(semidiameters) for ld in self.distance_measurements]
            lin_func = np.poly1d(np.polyfit(times, angles, 1))
            time = Time(ts, statistics.fmean(ld.time.tt for ld in self.distance_measurements))
            return Angle(lin_func(time.tt)), time

    def update_all(self):
        self.lunar_alt.correct(self.env)
        self.body_alt.correct(self.env)
        self.clear_ld()
        self.set_gmt_from_ld()

    def clear_ld(self):
        if self.lunar_alt.alt == None or self.body_alt.alt == None or len(self.distance_measurements) == 0:
            return
        self.uncleared_avg_ld, self.chronometer_time = self.compute_avg_ld_measurement()
        uncleared_lunar_alt = self.lunar_alt.alt.apparent_center()
        uncleared_body_alt = self.body_alt.alt.apparent_center()
        cleared_lunar_alt = self.lunar_alt.alt.observed
        cleared_body_alt = self.body_alt.alt.observed
        self.cleared_avg_ld = Angle(math.acos(
            ((math.cos(cleared_lunar_alt) * math.cos(cleared_body_alt) / (math.cos(uncleared_lunar_alt) * math.cos(uncleared_body_alt)))
             * (math.cos(self.uncleared_avg_ld) + math.cos(uncleared_lunar_alt + uncleared_body_alt)))
            - math.cos(cleared_lunar_alt + cleared_body_alt)))
        #prod_sin_cleared = math.sin(cleared_lunar_alt) * math.sin(cleared_body_alt)
        #self.cleared_avg_ld = Angle(math.acos(
        #    (math.cos(self.uncleared_avg_ld) * prod_sin_cleared / (math.sin(uncleared_lunar_alt) * math.sin(uncleared_body_alt)))
        #    + (math.cos(cleared_lunar_alt) * math.cos(cleared_body_alt))
        #    - (prod_sin_cleared / (math.tan(uncleared_lunar_alt) * math.tan(uncleared_body_alt)))))

    def ld_at(self, time):
        return Angle(
            self.moon.at(time).apparent_pos.separation_from(self.body.at(time).apparent_pos).radians)

    def set_gmt_from_ld(self):
        if self.chronometer_time == None:
            return
        t_floor = self.chronometer_time - 1/48 # minus 30 minutes
        t_ceil = self.chronometer_time + 1/48 # plus 30 minutes
        ld_floor = self.ld_at(t_floor)
        ld_ceil = self.ld_at(t_ceil)
        a = (t_ceil - t_floor)/(ld_ceil - ld_floor)
        b = t_floor - a * ld_floor
        self.gmt = b + a * self.cleared_avg_ld

    def update_and_print(self):
        self.update_all()
        self.print()

    def print(self):
        print(self.env)
        print("Moon")
        if self.lunar_alt.alt == None:
            print("    None")
        else:
            print("   ", self.lunar_alt.measured)
            print(f"    geocentric sd = {self.lunar_alt.alt.sd:min:.1f}, augmented sd = {self.lunar_alt.alt.sd_topo():min:.1f}")
            print("    Ha (center) =", format(self.lunar_alt.alt.apparent_center(), "ddm:.1f"))
            print("    Ho =", format(self.lunar_alt.alt.observed, "ddm:.1f"))
        print("----------------------")
        print(f"{self.body.name}")
        if self.body_alt.alt == None:
            print("    None")
        else:
            print("   ", self.body_alt.measured)
            print(f"    sd = {self.body_alt.alt.sd:min:.1f}")
            print("    Ha (center) =", format(self.body_alt.alt.apparent_center(), "ddm:.1f"))
            print("    Ho =", format(self.body_alt.alt.observed, "ddm:.1f"))
        print("----------------------")
        if self.chronometer_time == None:
            print("Average chronometer time: None")
        else:
            print("Average chronometer time: {} UTC".format(self.chronometer_time.utc_datetime()))
        print("Measured lunar distances:")
        if len(self.distance_measurements) == None:
            print("None")
        for ld in self.distance_measurements:
            print("   ", ld)
        if self.uncleared_avg_ld != None:
            print(f"Avg apparent LD = {self.uncleared_avg_ld:ddm:.1f}")
        if self.cleared_avg_ld != None:
            print(f"Avg cleared LD = {self.cleared_avg_ld:ddm:.1f}")
        print("----------------------")
        if self.gmt != None:
            print(f"Computed GMT: {self.gmt.utc_datetime()} UTC")
            print(f"(LD at that time: {self.ld_at(self.gmt):ddm:.1f})")
            #print(f"LD at {(self.chronometer_time - 1/48).utc_datetime()}: {self.ld_at(self.chronometer_time - 1/48)}")
            #print(f"LD at {(self.chronometer_time + 1/48).utc_datetime()}: {self.ld_at(self.chronometer_time + 1/48)}")
