import datetime
import math

from .angle import Angle
from . import spherical

class LineOfPosition:
    def __init__(self, pos: spherical.Pos, normal: Angle, time: datetime.datetime):
        self.pos = pos
        self.normal = normal
        self.time = time

class CircleOfPosition:
    def __init__(self, circle: spherical.Circle, time: datetime.datetime):
        self.circle = circle
        self.time = time

    def from_gp_and_observed_alt(gp, alt, time):
        return CircleOfPosition(spherical.Circle(gp, Angle.d90() - alt), time)

    def apx_lop_from(self, assumed_position: spherical.Pos) -> LineOfPosition:
        alt_az = spherical.alt_az(assumed_position, self.circle.centre)

        dist = Angle.d90() - alt_az.alt() - self.circle.radius

        x = assumed_position + spherical.Pos(math.sin(alt_az.az()) * dist, math.cos(alt_az.az()) * dist);

        return LineOfPosition(x, alt_az.az(), self.time)
