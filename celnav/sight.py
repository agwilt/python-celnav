from .base import *
from .angle import Angle
from .gp import Body, BodyAtTime
from skyfield.timelib import Time

import datetime
import math
import statistics
import numpy as np
import copy

def dip(height_of_eye: Metre) -> Angle:
    return Angle.from_minutes(1.76 * math.sqrt(height_of_eye))

class Environment:
    Pressure_hPa = typing.NewType('Pressure_hPa', float)

    class Temperature_K(float):
        def from_celsius(x_celsius):
            return __class__(x_celsius + 273.15)
        def to_celsius(self):
            return self - 273.15
        def __format__(self, format_string):
            raise NotImplementedError()

    def __init__(self):
        self.index_error = Angle(0) # TODO: Possibly add frame error function and allow persistent sextant errors
        self.height_of_eye = Metre(0) # metres
        self.temperature = Environment.Temperature_K.from_celsius(10)
        self.pressure = Environment.Pressure_hPa(1010)

    def __str__(self):
        return "Index Error {:min:.1f}, HoE {:.1f}m (Dip {:min:.1f}), Temperature {:.0f}°C, Pressure {:.0f}hPa".format(
                self.index_error, self.height_of_eye, dip(self.height_of_eye), self.temperature.to_celsius(),
                self.pressure)

    def copy(self):
        return copy.copy(self)

class Limb(enum.Enum):
    LOWER = 1
    CENTRE = 2
    UPPER = 3
    def nice_str(self):
        return {Limb.LOWER:'lower limb', Limb.CENTRE:'centre', Limb.UPPER:'upper limb'}[self]
    @staticmethod
    def from_char(c: str):
        return {'l':Limb.LOWER, 'c':Limb.CENTRE, 'u':Limb.UPPER}[c.lower()]

class AltitudeMeasurement:
    def __init__(self, limb: Limb, sextant_altitude: Angle, time: Time, comment: str = ""):
        self.limb = limb
        self.sextant_altitude = sextant_altitude
        self.time = time
        self.comment = comment

    def __str__(self):
        s = "{}: {:dms:02.0f} at {} UTC".format(self.limb.nice_str(), self.sextant_altitude,
                                                       self.time.utc_datetime())
                                                       #self.time, self.time.microsecond // 1e5)
        if self.comment:
            s += ', "' + self.comment + '"'
        return s

class ObservedAltitude:
    def __init__(self, altitude: AltitudeMeasurement, env: Environment, body: Body):
        body_at_time = BodyAtTime(body, altitude.time)
        self.hp = body_at_time.hp()
        self.sd = body_at_time.sd()
        self.measured = altitude
        self.apparent = self.measured.sextant_altitude + env.index_error - dip(env.height_of_eye) # index error, dip
        self.topocentric = self.apparent\
                           - Angle.from_minutes(
                                (env.pressure / 1010)
                                * (283.15 / env.temperature)
                                / math.tan(self.apparent + Angle.from_degrees(7.31 / (self.apparent.degrees() + 4.4)))
                            ) # refraction
        self.geocentric = self.topocentric + math.cos(self.topocentric) * self.hp # parallax
        self.observed = self.geocentric + self.limb_correction(self.sd) # limb correction

    def __str__(self):
        return "{} (Hsex = {:ddm:.1f}, Happ = {:ddm:.1f}, Htopo = {:ddm:.1f}, hp = {:min:.1f}, Hgeo = {:ddm:.1f}, sd = {:min:.1f}, Ho = {:ddm:.1f})".format(
                self.measured, self.measured.sextant_altitude, self.apparent, self.topocentric, self.hp, self.geocentric, self.sd, self.observed)

    def ho_and_t(self):
        return self.observed, self.measured.time

    def sd_topo(self):
        if self.sd == 0:
            return 0
        return self.hp * math.sin(self.sd) / math.sin(self.hp) * (1 + math.sin(self.hp) * math.sin(self.topocentric))

    def limb_correction(self, sd):
        return {Limb.LOWER:sd, Limb.CENTRE:Angle(0), Limb.UPPER:-sd}[self.measured.limb]

    def apparent_center(self):
        return self.apparent + self.limb_correction(self.sd_topo()) # limb correction

class Sight:
    def __init__(self, body:Body, env: Environment):
        self.env = env
        self.body = body
        self.sights: list[AltitudeMeasurement] = []
        self.corrected_sights: list[ObservedAltitude] = []

    def print(self):
        print("{}".format(self.env))
        print("Altitude observations:" + (" None" if not len(self.sights) else ""))
        for sight in self.corrected_sights:
            print("  " + str(sight))

    def body_str(self):
        return self.body.name

    def mean_time(self):
        return Time(ts, statistics.fmean(sight.time.tt for sight in self.sights))

    def correct_altitudes(self):
        self.corrected_sights = [ObservedAltitude(alt, self.env, self.body) for alt in self.sights]

    def average_corrected_sight(self):
        self.correct_altitudes()
        assert len(self.corrected_sights)
        if len(self.corrected_sights) == 1:
            return self.corrected_sights[0].ho_and_t()
        else:
            times = [sight.time.tt for sight in self.sights]
            altitudes = [sight.observed for sight in self.corrected_sights]
            lin_func = np.poly1d(np.polyfit(times, altitudes, 1))
            time = self.mean_time()
            return Angle(lin_func(time.tt)), time
