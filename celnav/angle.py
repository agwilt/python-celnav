import math

class Angle(float):
    format_spec = "ddm" # options: dd, ddm, dms, rad, min

    def __new__(cls, x = None):
        if (type(x) == str):
            x = x.strip()
            if x[-1] == '°': # dd
                return float.__new__(cls, math.radians(float(x[:-1])))
            elif x[-1] == '\'': # ddm
                minutes = 0
                if (deg_pos := x.find('°')) != -1:
                    minutes = abs(float(x[:deg_pos])) * 60
                minutes += abs(float(x[deg_pos+1:-1]))
                if '-' in x:
                    minutes = -minutes
                return float.__new__(cls, math.radians(minutes / 60))
            elif x[-1] == '"': # dms
                seconds = 0
                if (min_pos := x.find('\'')) != -1:
                    if (deg_pos := x.find('°')) != -1:
                        seconds = abs(float(x[:deg_pos])) * 3600
                    seconds += abs(float(x[deg_pos+1:min_pos])) * 60
                else: # no minutes, so make sure there are no degrees either
                    if ('°' in x):
                        raise ValueError("DMS string cannot give degrees and seconds without minutes")
                seconds += abs(float(x[min_pos+1:-1]))
                if '-' in x:
                    seconds = -seconds
                return float.__new__(cls, math.radians(seconds / 3600))
        return float.__new__(cls, x)

    def from_degrees(degrees: float):
        return Angle(math.radians(degrees))

    def from_radians(radians: float):
        return Angle(radians)

    def from_minutes(minutes: float):
        return Angle.from_degrees(minutes / 60)

    def acos(x: float):
        return Angle(math.acos(x))

    def asin(x: float):
        return Angle(math.asin(x))

    def atan(x: float):
        return Angle(math.atan(x))

    def radians(self):
        return float(self)

    def degrees(self) -> float:
        return math.degrees(self)

    def abs_ddm(self): # degrees, minutes
        abs_deg = abs(self.degrees())
        d, m = divmod(abs_deg * 60, 60)
        return round(d), m

    def abs_dms(self): # degrees, minutes, seconds
        abs_deg = abs(self.degrees())
        m, s = divmod(abs_deg * 3600, 60)
        d, m = divmod(m, 60)
        return round(d), round(m), s

    def __format__(self, format_spec):
        # set default format if unset
        angle_fmt, _, float_fmt = format_spec.partition(':')
        if not angle_fmt:
            angle_fmt = Angle.format_spec

        if angle_fmt == "dd":
            return format(self.degrees(), float_fmt) + "°"
        elif angle_fmt == "min":
            return format(self.degrees() * 60, float_fmt) + "'"
        elif angle_fmt == "ddm":
            d, m = self.abs_ddm()
            return ("{}{:d}°{:" + float_fmt + "}'").format('-' if self < 0 else '', d, m)
        elif angle_fmt == "dms":
            d, m, s = self.abs_dms()
            return ("{}{:d}°{:02d}'{:" + float_fmt + "}\"").format('-' if self < 0 else '', d, m, s)
        elif angle_fmt == "rad":
            return float.__format__(self, float_fmt)
        else:
            raise ValueError("Invalid format: " + angle_fmt)

    def __repr__(self):
        return "celnav.Angle(" + repr(format(self)) + ")"

    def __str__(self):
        return format(self)

    def normalised(self):
        return (self % Angle.d360())

    def __add__(self, x):
        if type(x) != type(self):
            raise ValueError("Cannot add angle to object of type " + repr(type(x)))
        return Angle(float.__add__(self, x))

    def __sub__(self, x):
        if type(x) != type(self):
            raise ValueError("Cannot subtract object of type " + repr(type(x)) + " from angle")
        return Angle(float.__sub__(self, x))

    def __mul__(self, x):
        if type(x) == type(self):
            raise ValueError("Cannot multiply two angles. That's just silly.")
        return Angle(float.__mul__(self, x))

    def __rmul__(self, x):
        return self * x

    def __truediv__(self, x):
        if type(x) == type(self):
            raise ValueError("Cannot divide angle by angle. That's just silly.")
        return Angle(float.__truediv__(self, x))

    def __mod__(self, x):
        if type(x) != type(self):
            raise TypeError("unsupported operand type(s) for %: " + type(self).__name__ + " and "
                            + type(x).__name__)
        return Angle(float.__mod__(self, x))


    def __floordiv__(self, x):
        raise TypeError("unsupported operand type(s) for //: " + type(self).__name__ + " and "
                        + type(x).__name__)

    def __abs__(self):
        return Angle(float.__abs__(self))

    def __neg__(self):
        return Angle(0) - self

    @staticmethod
    def d0(): return Angle(0)

    @staticmethod
    def d90(): return Angle(math.pi / 2)

    @staticmethod
    def d180(): return Angle(math.pi)

    @staticmethod
    def d360(): return Angle(math.tau)
