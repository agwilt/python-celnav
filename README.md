Python-based celnav stuff, initially for use on laptops and phones.
Maybe also a battery-powered ARM-based board at some point in the future ...

The main python module is in `celnav`.

`interactive_celnav` is an interactive shell, to allow the relevant computations without having to learn
python or the `celnav` module.

Similarly, `interactive_lunar` is a script to do quick lunar distance sights.
