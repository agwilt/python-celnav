#!/usr/bin/env python3

import unittest
import numpy.testing
import math
import random

from celnav.angle import Angle
from celnav import *

class TestAngle(unittest.TestCase):
    def test_string_constructor(self):
        self.assertEqual(Angle("-4°3'    2\" "), math.radians(-4 - 3/60 - 2/3600))

    def test_construct_after_print(self):
        for x in [0, math.tau, -math.tau] + [random.uniform(-math.tau, math.tau) for i in range(10)]:
            x = Angle(x)
            for format_spec in ("dd", "ddm", "dms", "rad"):
                numpy.testing.assert_allclose(x, Angle(format(x, format_spec)), rtol=0, atol=1e-14)

    def test_minutes_to_radians(self):
        numpy.testing.assert_allclose(math.pi, Angle.from_minutes(180*60))

class TestSpherical(unittest.TestCase):
    def test_suffix_to_sign(self):
        self.assertEqual(spherical.suffix_to_sign("5°W"), "-5°")
        self.assertEqual(spherical.suffix_to_sign("-5°W"), "5°")
        self.assertEqual(spherical.suffix_to_sign("-5°E"), "-5°")
        self.assertEqual(spherical.suffix_to_sign("-5°W"), "5°")

if __name__ == "__main__":
    unittest.main()
